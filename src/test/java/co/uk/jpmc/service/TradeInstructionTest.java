package co.uk.jpmc.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Currency;

import org.junit.Test;

import co.uk.jpmc.beans.TradeInstruction;
import co.uk.jpmc.beans.TradeType;

public class TradeInstructionTest {

    @Test
    public void testTradeAmountCalc() throws Exception {
        final BigDecimal agreedFx = BigDecimal.valueOf(0.73);
        final BigDecimal pricePerUnit = BigDecimal.valueOf(100);
        final Currency AUD = Currency.getInstance("AUD");
        final int units = 50;

    	final TradeInstruction tradeInstruction = new TradeInstruction("Kookaburra-Bat", TradeType.BUY, agreedFx, 
        		AUD, LocalDate.of(2018, 8, 3), LocalDate.of(2018, 8, 13), units, pricePerUnit);

        assertEquals(agreedFx, tradeInstruction.getAgreedFx());
        assertEquals(AUD, tradeInstruction.getCurrency());
        assertEquals(pricePerUnit, tradeInstruction.getPricePerUnit());
        assertEquals(units, tradeInstruction.getUnits());

        final BigDecimal calculatedTradeAmount = pricePerUnit.multiply(agreedFx).multiply(BigDecimal.valueOf(units)).setScale(2, RoundingMode.HALF_EVEN);
        assertEquals(calculatedTradeAmount, tradeInstruction.getTradeAmount());
    }
}