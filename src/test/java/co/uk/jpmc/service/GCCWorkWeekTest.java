package co.uk.jpmc.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class GCCWorkWeekTest {
    private WorkWeek workWeek;
 
    @Before
    public void setUp() throws Exception {
        workWeek = GCCWorkWeek.getGCCWorkWeek();
    }

    /*
     * Cheat sheet for Dates in AUGUST 2018
     * 10: Friday; 11: Saturday; 12: Sunday; 13: Monday
     */

    /** Monday - Global working day */
    @Test
    public void testFindFirstWorkingDayFromTrueWorkingDay() throws Exception {
        final LocalDate mondayAsWorkingDay = LocalDate.of(2018, 8, 13);
        assertEquals(mondayAsWorkingDay, workWeek.getWorkingDay(mondayAsWorkingDay));
    }

    /** Friday - only GCC weekend */
    @Test
    public void testFindFirstWorkingDayFromFridayWeekend() throws Exception {
        final LocalDate fridayAsWeekend = LocalDate.of(2018, 8, 10);
        assertEquals(LocalDate.of(2018, 8, 12), workWeek.getWorkingDay(fridayAsWeekend));

    }

    /** Saturday - Global weekend */
    @Test
    public void testFindFirstWorkingDayFromSaturdayWeekend() throws Exception {
        final LocalDate saturdayAsWeekend = LocalDate.of(2018, 8, 11);
        assertEquals(LocalDate.of(2018, 8, 12), workWeek.getWorkingDay(saturdayAsWeekend));
    }

    /** Sunday - only GCC working day */
    @Test
    public void testFindFirstWorkingDayFromSunday() throws Exception {
        final LocalDate sundayAsNonWeekend = LocalDate.of(2018, 8, 12);
        assertEquals(sundayAsNonWeekend, workWeek.getWorkingDay(sundayAsNonWeekend));
    }

}
