package co.uk.jpmc.service;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Currency;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.junit.Test;

import co.uk.jpmc.beans.Rank;
import co.uk.jpmc.beans.TradeInstruction;
import co.uk.jpmc.beans.TradeType;
import co.uk.jpmc.exception.JPMCRuntimeException;

public class SettlementCalculationServiceTest {
	/*
     * Cheat sheet for Dates in AUGUST 2018
     * 10: Friday; 11: Saturday; 12: Sunday; 13: Monday; and so on
     * Buy (Outgoing) or Sell (Incoming)
     */
    private static final LocalDate SATURDAY  = LocalDate.of(2018, 8, 11);
    private static final LocalDate SUNDAY    = LocalDate.of(2018, 8, 12);
    private static final LocalDate MONDAY    = LocalDate.of(2018, 8, 13);
    private static final LocalDate TUESDAY 	 = LocalDate.of(2018, 8, 14);
    private static final LocalDate WEDNESDAY = LocalDate.of(2018, 8, 15);

    private static Set<TradeInstruction> generateDummyTradeInstructions() {
        final Set<TradeInstruction> allTradeInstructions = new HashSet<>();

        /*
         * EUR Zone: Saturday, Sunday is Weekend
         */
        allTradeInstructions.add(new TradeInstruction("Apple", TradeType.BUY, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), 
        		LocalDate.of(2018, 8, 3), MONDAY, 10, BigDecimal.valueOf(100.25)));
        allTradeInstructions.add(new TradeInstruction("Banana", TradeType.SELL, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), 
        		LocalDate.of(2018, 8, 3), SUNDAY, 10, BigDecimal.valueOf(150.5)));
        allTradeInstructions.add(new TradeInstruction("Citrus", TradeType.BUY, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), 
        		LocalDate.of(2018, 8, 3), SATURDAY, 10, BigDecimal.valueOf(300.6)));

        /*
         * GCC: Friday, Saturday is Weekend, Sunday is Working day
         */
        allTradeInstructions.add(new TradeInstruction("Elderberry", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
        		LocalDate.of(2018, 8, 3), TUESDAY, 10, BigDecimal.valueOf(650.5)));
        allTradeInstructions.add(new TradeInstruction("Fig", TradeType.SELL, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
        		LocalDate.of(2018, 8, 3), TUESDAY, 10, BigDecimal.valueOf(40.8)));
        allTradeInstructions.add(new TradeInstruction("Grape", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
        		LocalDate.of(2018, 8, 3), WEDNESDAY, 10, BigDecimal.valueOf(261.25)));
        allTradeInstructions.add(new TradeInstruction("HoneyBerry", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
        		LocalDate.of(2018, 8, 3), MONDAY, 10, BigDecimal.valueOf(350.5)));

        SettlementDateService.calculateAllSettlementDates(allTradeInstructions);
        return allTradeInstructions;
    }

    /**
     * Checking rank for incoming trades, total size, size by a particular day, rank by a particular day
     * @throws JPMCRuntimeException
     */
    @Test
    public void tesCalculatetDailyIncomingRanking() throws JPMCRuntimeException {
        final Map<LocalDate, LinkedList<Rank>> dailyIncomingRanking = SettlementCalculationService.calculateDailyIncomingRanking(generateDummyTradeInstructions());
        assertTrue(dailyIncomingRanking.get(MONDAY).contains(new Rank(1, "Banana", MONDAY)));
        assertTrue(dailyIncomingRanking.get(TUESDAY).contains(new Rank(1, "Fig", TUESDAY)));
        assertEquals(2, dailyIncomingRanking.size());
        assertEquals(1, dailyIncomingRanking.get(MONDAY).size());
        assertEquals(1, dailyIncomingRanking.get(TUESDAY).size());
    }

    @Test
    public void testCalculateDailyOutgoingRanking() throws JPMCRuntimeException {
        final Map<LocalDate, LinkedList<Rank>> dailyOutgoingRanking = SettlementCalculationService.calculateDailyOutgoingRanking(generateDummyTradeInstructions());
        assertTrue(dailyOutgoingRanking.get(WEDNESDAY).contains(new Rank(1, "Grape", WEDNESDAY)));
        assertEquals(1, dailyOutgoingRanking.get(WEDNESDAY).size());
        assertTrue(dailyOutgoingRanking.get(TUESDAY).contains(new Rank(1, "Elderberry", TUESDAY)));
        assertEquals(1, dailyOutgoingRanking.get(TUESDAY).size());
        assertTrue(dailyOutgoingRanking.get(MONDAY).contains(new Rank(1, "Citrus", MONDAY)));
        assertTrue(dailyOutgoingRanking.get(MONDAY).contains(new Rank(2, "Apple", MONDAY)));
        assertTrue(dailyOutgoingRanking.get(MONDAY).contains(new Rank(3, "HoneyBerry", MONDAY)));
        assertEquals(3, dailyOutgoingRanking.get(MONDAY).size());
        assertEquals(3, dailyOutgoingRanking.size());
    }

    @Test
    public void testCalculateDailyIncomingAmount() throws JPMCRuntimeException {
        final Map<LocalDate, BigDecimal> dailyIncomingAmount = SettlementCalculationService.calculateDailyIncomingAmount(generateDummyTradeInstructions());
        assertEquals(2, dailyIncomingAmount.size());
        assertTrue(Objects.equals(dailyIncomingAmount.get(MONDAY), BigDecimal.valueOf(1745.80).setScale(2, RoundingMode.HALF_EVEN)));
        assertTrue(Objects.equals(dailyIncomingAmount.get(TUESDAY), BigDecimal.valueOf(89.76).setScale(2, RoundingMode.HALF_EVEN)));
    }

    @Test
    public void testCalculateDailyOutgoingAmount() throws JPMCRuntimeException {
        final Map<LocalDate, BigDecimal> dailyOutgoingAmount = SettlementCalculationService.calculateDailyOutgoingAmount(generateDummyTradeInstructions());
        assertEquals(3, dailyOutgoingAmount.size());
        assertTrue(Objects.equals(dailyOutgoingAmount.get(MONDAY), BigDecimal.valueOf(5420.96).setScale(2, RoundingMode.HALF_EVEN)));
        assertTrue(Objects.equals(dailyOutgoingAmount.get(TUESDAY), BigDecimal.valueOf(1431.10).setScale(2, RoundingMode.HALF_EVEN)));
        assertTrue(Objects.equals(dailyOutgoingAmount.get(WEDNESDAY), BigDecimal.valueOf(574.75).setScale(2, RoundingMode.HALF_EVEN)));
    }

}
