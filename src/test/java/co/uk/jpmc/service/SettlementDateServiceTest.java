package co.uk.jpmc.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

import org.junit.Test;

import co.uk.jpmc.beans.TradeInstruction;
import co.uk.jpmc.beans.TradeType;
import co.uk.jpmc.exception.JPMCRuntimeException;

public class SettlementDateServiceTest {

	/*
     * Cheat sheet for Dates in AUGUST 2018
     * 10: Friday; 11: Saturday; 12: Sunday; 13: Monday
     */

    /** Sunday - Global weekend, GCC working day */
    @Test
    public void calculateSettlementDateFromSunday() throws Exception {
        final LocalDate sunday = LocalDate.of(2018, 8, 12);
        final TradeInstruction tradeInstruction = new TradeInstruction("KiwiFruit", TradeType.BUY, BigDecimal.valueOf(0.00), 
        		Currency.getInstance("USD"), LocalDate.of(2018, 8, 3), sunday, 200, BigDecimal.valueOf(120.25));
        SettlementDateService.calculateSettlementDate(tradeInstruction);
        assertEquals(LocalDate.of(2018, 8, 13), tradeInstruction.getSettlementDate());
    }

    /** Sunday - GCC working day, Global weekend */
    @Test
    public void calculateSettlementDateFromSundayForGCC() throws Exception {
        final LocalDate sunday = LocalDate.of(2018, 8, 12);
        final TradeInstruction tradeInstruction = new TradeInstruction("Dates", TradeType.BUY, BigDecimal.valueOf(0.22), 
        		Currency.getInstance("KWD"), LocalDate.of(2018, 8, 3), sunday, 450, BigDecimal.valueOf(29.5));
        SettlementDateService.calculateSettlementDate(tradeInstruction);
        assertEquals(sunday, tradeInstruction.getSettlementDate());
    }

    /** Monday - Global working day */
    @Test
    public void calculateSettlementDateFromGlobalWorkingDay() throws JPMCRuntimeException {
        final LocalDate mondayAsWorkingDay = LocalDate.of(2018, 8, 13);
        final TradeInstruction tradeInstruction = new TradeInstruction("KiwiFruit", TradeType.BUY, BigDecimal.valueOf(0.00), 
        		Currency.getInstance("USD"), LocalDate.of(2018, 8, 3), mondayAsWorkingDay, 200, BigDecimal.valueOf(120.25));
        SettlementDateService.calculateSettlementDate(tradeInstruction);
        assertEquals(mondayAsWorkingDay, tradeInstruction.getSettlementDate());
    }

    /** Friday - GCC weekend, Global working day */
    @Test
    public void calculateSettlementDateFromFridayForGCC() throws Exception {
        final LocalDate friday = LocalDate.of(2018, 8, 10);
        final TradeInstruction tradeInstruction = new TradeInstruction("Banana", TradeType.BUY, BigDecimal.valueOf(0.22), 
        		Currency.getInstance("AED"), LocalDate.of(2018, 8, 3), friday, 450, BigDecimal.valueOf(150.5));
        SettlementDateService.calculateSettlementDate(tradeInstruction);
        assertEquals(LocalDate.of(2018, 8, 12), tradeInstruction.getSettlementDate());
    }
}
