package co.uk.jpmc.beans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Currency;

/**
 * Instruction from clients for buying or selling
 */

public class TradeInstruction {

    /** A financial entity whose shares are to be bought or sold */
    private final String entity;

    /** Instruction action, Buy (B) or Sell (S) */
    private final TradeType tradeType;

    /** The foreign exchange rate with respect to USD that was agreed */
    private final BigDecimal agreedFx;

    /** Currency of the instruction*/
    private final Currency currency;

    /** Date on which the instruction was sent by various clients */
    private final LocalDate instructionDate;

    /** Date on which the client wished for the instruction to be settled with respect to Instruction Date */
    private LocalDate settlementDate;

    /** Number of shares to be bought or sold */
    private final int units;

    /** Price of One (1) unit */
    private final BigDecimal pricePerUnit;

    /** Report special: Total trade amount in USD */
    private final BigDecimal tradeAmount;

    public TradeInstruction(String entity, TradeType tradeType, BigDecimal agreedFx, Currency currency, LocalDate instructionDate, LocalDate settlementDate, 
        int units, BigDecimal pricePerUnit) {
        this.entity = entity;
        this.tradeType = tradeType;
        this.agreedFx = agreedFx;
        this.currency = currency;
        this.instructionDate = instructionDate;
        this.settlementDate = settlementDate;
        this.units = units;
        this.pricePerUnit = pricePerUnit;
        this.tradeAmount = getTradeAmount(this);
    }

    private static BigDecimal getTradeAmount(TradeInstruction tradeInstruction) {
        return tradeInstruction.getPricePerUnit()
                .multiply(BigDecimal.valueOf(tradeInstruction.getUnits()))
                .multiply(tradeInstruction.getAgreedFx());
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }


    public void setSettlementDate(LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }


    public String getEntity() {
        return entity;
    }


    public TradeType getTradeType() {
        return tradeType;
    }


    public BigDecimal getAgreedFx() {
        return agreedFx;
    }


    public Currency getCurrency() {
        return currency;
    }


    public LocalDate getInstructionDate() {
        return instructionDate;
    }


    public int getUnits() {
        return units;
    }


    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }


    public BigDecimal getTradeAmount() {
        return tradeAmount.setScale(2, RoundingMode.HALF_EVEN);
    }

    //TODO: HashCode needed 
    @Override
    public String toString() {
        return entity;
    }
}
