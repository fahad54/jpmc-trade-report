package co.uk.jpmc.beans;

import java.util.Arrays;

public enum TradeType {
    BUY("B"),
    SELL("S");

    private String detail;

    TradeType(String tradeTypeDetail) {
        this.detail = tradeTypeDetail;
    }

    public String getText() {
        return this.detail;
    }

    /** Named the function to fromString() to get benefits from Java, like, direct conversion of types at HeaderParam annotation */
    public static TradeType fromString(String tradeDetail) {
        return Arrays.stream(TradeType.values())
            .filter(v -> v.detail.equals(tradeDetail))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Invalid TradeType - " + tradeDetail));
    }
}
