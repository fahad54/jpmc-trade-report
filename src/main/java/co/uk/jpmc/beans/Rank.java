package co.uk.jpmc.beans;

import java.time.LocalDate;

public class Rank {
    /** Rank in the report */
    private int rank;

    /** Entity of the instruction */
    private String entity;

    /** Date settled */
    private LocalDate date;

    public Rank(int rank, String entity, LocalDate date) {
        this.rank = rank;
        this.entity = entity;
        this.date = date;
    }
    
    public Rank() {
	}

	public Rank getBeanInstance() {
        return new Rank();
    }


    /** Making sure the Java Contract of hashCode and equals is followed */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /** Comparing all the properties before declaring it equal */
    @Override
    public boolean equals(Object obj) {
        final Rank other = (Rank) obj;

        return other.getRank() == this.getRank() && other.getEntity()
            .equals(this.getEntity()) && other.getDate()
            .equals(this.getDate());
    }

    @Override
    public String toString() {
        return getEntity() + " - " + "[" + getRank() + "]";
    }

    public int getRank() {
        return rank;
    }

    public String getEntity() {
        return entity;
    }

    public LocalDate getDate() {
        return date;
    }

}
