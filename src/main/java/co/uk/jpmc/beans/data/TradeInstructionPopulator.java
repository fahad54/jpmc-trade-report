package co.uk.jpmc.beans.data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;

import co.uk.jpmc.beans.TradeInstruction;
import co.uk.jpmc.beans.TradeType;

public class TradeInstructionPopulator {
    private static final LocalDate SATURDAY  = LocalDate.of(2018, 8, 11);
    private static final LocalDate SUNDAY    = LocalDate.of(2018, 8, 12);
    private static final LocalDate MONDAY    = LocalDate.of(2018, 8, 13);
    private static final LocalDate TUESDAY 	 = LocalDate.of(2018, 8, 14);
    private static final LocalDate WEDNESDAY = LocalDate.of(2018, 8, 15);

	public static Set<TradeInstruction> getPopulatedInstructions() {
		return new HashSet<>(Arrays.asList(

				new TradeInstruction("Apple", TradeType.BUY, BigDecimal.valueOf(0.50), Currency.getInstance("SGD"), LocalDate.of(2018, 8, 3), MONDAY, 200, BigDecimal.valueOf(100.25)),
				new TradeInstruction("Banana", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("AED"), LocalDate.of(2018, 8, 3), SATURDAY, 450, BigDecimal.valueOf(150.5)),
				new TradeInstruction("Citrus", TradeType.SELL, BigDecimal.valueOf(0.27), Currency.getInstance("SAR"), LocalDate.of(2018, 8, 3), SATURDAY, 150, BigDecimal.valueOf(300.6)),
				new TradeInstruction("Durian", TradeType.SELL, BigDecimal.valueOf(0.00), Currency.getInstance("USD"), LocalDate.of(2018, 8, 3), MONDAY, 200, BigDecimal.valueOf(200.55)),
				new TradeInstruction("Elderberry", TradeType.SELL, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), LocalDate.of(2018, 8, 3), SUNDAY, 450, BigDecimal.valueOf(650.5)),
				new TradeInstruction("Fig", TradeType.BUY, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), LocalDate.of(2018, 8, 3), MONDAY, 150, BigDecimal.valueOf(40.8)),
				new TradeInstruction("Grape", TradeType.SELL, BigDecimal.valueOf(0.50), Currency.getInstance("SGD"), LocalDate.of(2018, 8, 3), MONDAY, 200, BigDecimal.valueOf(261.25)),
				new TradeInstruction("HoneyBerry", TradeType.SELL, BigDecimal.valueOf(0.22), Currency.getInstance("AED"), LocalDate.of(2018, 8, 3), SATURDAY, 450, BigDecimal.valueOf(350.5)),
				new TradeInstruction("JackFruit", TradeType.BUY, BigDecimal.valueOf(0.27), Currency.getInstance("SAR"), LocalDate.of(2018, 8, 3), SUNDAY, 150, BigDecimal.valueOf(60.8)),
				new TradeInstruction("KiwiFruit", TradeType.BUY, BigDecimal.valueOf(0.00), Currency.getInstance("USD"), LocalDate.of(2018, 8, 3), SUNDAY, 200, BigDecimal.valueOf(120.25)),
				new TradeInstruction("Lemon", TradeType.BUY, BigDecimal.valueOf(0.25), Currency.getInstance("QAR"), LocalDate.of(2018, 8, 3), SUNDAY, 450, BigDecimal.valueOf(550.5)),
				new TradeInstruction("Mango", TradeType.SELL, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), LocalDate.of(2018, 8, 3), SUNDAY, 150, BigDecimal.valueOf(90.8))
				));
	}

	public static Set<TradeInstruction> getPopulatedInstructionsAsUsedInTests() {
		return new HashSet<>(Arrays.asList(

        		new TradeInstruction("Apple", TradeType.BUY, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), 
                		LocalDate.of(2018, 8, 3), MONDAY, 10, BigDecimal.valueOf(100.25)),
                new TradeInstruction("Banana", TradeType.SELL, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), 
                		LocalDate.of(2018, 8, 3), SUNDAY, 10, BigDecimal.valueOf(150.5)),
                new TradeInstruction("Citrus", TradeType.BUY, BigDecimal.valueOf(1.16), Currency.getInstance("EUR"), 
                		LocalDate.of(2018, 8, 3), SATURDAY, 10, BigDecimal.valueOf(300.6)),
                new TradeInstruction("Elderberry", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
                		LocalDate.of(2018, 8, 3), TUESDAY, 10, BigDecimal.valueOf(650.5)),
                new TradeInstruction("Fig", TradeType.SELL, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
                		LocalDate.of(2018, 8, 3), TUESDAY, 10, BigDecimal.valueOf(40.8)),
                new TradeInstruction("Grape", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
                		LocalDate.of(2018, 8, 3), WEDNESDAY, 10, BigDecimal.valueOf(261.25)),
                new TradeInstruction("HoneyBerry", TradeType.BUY, BigDecimal.valueOf(0.22), Currency.getInstance("QAR"), 
                		LocalDate.of(2018, 8, 3), MONDAY, 10, BigDecimal.valueOf(350.5))
				));
	}
}
