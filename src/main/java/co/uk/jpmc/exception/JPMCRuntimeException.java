package co.uk.jpmc.exception;

public class JPMCRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public JPMCRuntimeException() { }

	public JPMCRuntimeException(String message) {
		super(message);
	}
}
