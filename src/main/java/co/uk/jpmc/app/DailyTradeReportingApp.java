package co.uk.jpmc.app;

import co.uk.jpmc.beans.data.TradeInstructionPopulator;
import co.uk.jpmc.service.ReportGenerationService;


public class DailyTradeReportingApp {
	/** 'final' keyword is used through out the application to explicitly define Immutable objects and properties */
	public static void main(String[] args) {
        /** Report printing service */
        final ReportGenerationService reportService = new ReportGenerationService();
        System.out.println(reportService.generateDailyTradeReport(TradeInstructionPopulator.getPopulatedInstructions()));
        System.out.println("\n------- Data as used in SettlementCalculationServiceTest  ------- \n");
        reportService.cleanReport();
        System.out.println(reportService.generateDailyTradeReport(TradeInstructionPopulator.getPopulatedInstructionsAsUsedInTests()));
    }

}
