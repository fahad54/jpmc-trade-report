package co.uk.jpmc.service;

import java.time.DayOfWeek;

public class GCCWorkWeek extends WorkWeek {

    private GCCWorkWeek() {
        super();
    }

    private static GCCWorkWeek gCCWorkWeek = null;

    public static GCCWorkWeek getGCCWorkWeek() {
        if (gCCWorkWeek == null) {
            gCCWorkWeek = new GCCWorkWeek();
        }
        return gCCWorkWeek;
    }

    /** Ideally this should be defined in properties or config level setting should be used */
    @Override
    protected void getWorkingDaysOfWeek() {
        this.workingDayMap.put(DayOfWeek.FRIDAY, false);
        this.workingDayMap.put(DayOfWeek.SATURDAY, false);
        this.workingDayMap.put(DayOfWeek.SUNDAY, true);
        this.workingDayMap.put(DayOfWeek.MONDAY, true);
        this.workingDayMap.put(DayOfWeek.TUESDAY, true);
        this.workingDayMap.put(DayOfWeek.WEDNESDAY, true);
        this.workingDayMap.put(DayOfWeek.THURSDAY, true);
    }

}
