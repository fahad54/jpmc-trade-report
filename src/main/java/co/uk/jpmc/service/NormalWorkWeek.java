package co.uk.jpmc.service;

import java.time.DayOfWeek;

public class NormalWorkWeek extends WorkWeek {

    private NormalWorkWeek() {
        super();
    }

    private static NormalWorkWeek normalWorkWeek = null;

    public static NormalWorkWeek getNormalWorkWeek() {
        if (normalWorkWeek == null) {
            normalWorkWeek = new NormalWorkWeek();
        }
        return normalWorkWeek;
    }

    /** Ideally this should be defined in properties or config level */
    @Override
    protected void getWorkingDaysOfWeek() {
        this.workingDayMap.put(DayOfWeek.SATURDAY, false);
        this.workingDayMap.put(DayOfWeek.SUNDAY, false);
        this.workingDayMap.put(DayOfWeek.MONDAY, true);
        this.workingDayMap.put(DayOfWeek.TUESDAY, true);
        this.workingDayMap.put(DayOfWeek.WEDNESDAY, true);
        this.workingDayMap.put(DayOfWeek.THURSDAY, true);
        this.workingDayMap.put(DayOfWeek.FRIDAY, true);
    }

}
