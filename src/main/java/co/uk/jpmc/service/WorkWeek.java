package co.uk.jpmc.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public abstract class WorkWeek {

    protected Map<DayOfWeek, Boolean> workingDayMap = new HashMap<>();
    
    /** Will be implemented according to the Weekends in Gulf Cooperation Council (GCC) and rest of the world */
    protected abstract void getWorkingDaysOfWeek();

    public WorkWeek() {
        getWorkingDaysOfWeek();
    }

    public LocalDate getWorkingDay(LocalDate day) {

        /** Working day check */
        if (workingDayMap.values().stream().noneMatch(days -> days)) {
            return null;
        }

        /** First working day check */
        return getWorkingDayRecursively(day);
    }

    private LocalDate getWorkingDayRecursively(LocalDate inputDate) {
        final DayOfWeek inputDay = inputDate.getDayOfWeek();

        /** If inputDate is a working day */
        if (workingDayMap.get(inputDay)) {
            return inputDate;
        } else {
            /** Else search for next working day */
            return getWorkingDayRecursively(inputDate.plusDays(1));
        }
    }
}
