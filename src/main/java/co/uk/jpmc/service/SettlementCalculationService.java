package co.uk.jpmc.service;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.reducing;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toSet;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import co.uk.jpmc.beans.Rank;
import co.uk.jpmc.beans.TradeInstruction;
import co.uk.jpmc.beans.TradeType;

/** Service that performs core function, i.e. Calculations of daily outgoing and incoming amounts based on agreedFX 
 * and ranks them according to provided logic */

public class SettlementCalculationService {
    /** Static immutable properties for amount calculation and rankings */
    private final static Predicate<TradeInstruction> outgoingTrade = tradeInstruction -> tradeInstruction.getTradeType().equals(TradeType.BUY);
    private final static Predicate<TradeInstruction> incomingTrade = tradeInstruction -> tradeInstruction.getTradeType().equals(TradeType.SELL);

    /** Low-level and hidden implementation for calculating daily amount, both incoming and outgoing*/
    private static Map<LocalDate, BigDecimal> calculateDailyTradeAmount(Set<TradeInstruction> tradeInstructions, Predicate<TradeInstruction> predicate)
    {
        return tradeInstructions.stream()
            .filter(predicate)
            .collect(groupingBy(TradeInstruction::getSettlementDate,
                mapping(TradeInstruction::getTradeAmount,
                    reducing(BigDecimal.ZERO, BigDecimal::add))));
    }

    /** Low-level and hidden implementation for deciding ranks */
    private static Map<LocalDate, LinkedList<Rank>> calculateTradeRanking(Set<TradeInstruction> tradeInstruction, Predicate<TradeInstruction> predicate)
    {
        final Map<LocalDate, LinkedList<Rank>> localRanking = new HashMap<>();
        tradeInstruction.stream()
        .filter(predicate)
        .collect(groupingBy(TradeInstruction::getSettlementDate, toSet()))
        .forEach((date, allDailyTradeInstructions) -> {
            final AtomicInteger rank = new AtomicInteger(1);
            final LinkedList<Rank> calculatedRanks = allDailyTradeInstructions.stream()
                .sorted((a, b) -> b.getTradeAmount().compareTo(a.getTradeAmount()))
                .map(currentTrade -> new Rank(rank.getAndIncrement(), currentTrade.getEntity(), date))
                .collect(toCollection(LinkedList::new));

            localRanking.put(date, calculatedRanks);
        });
        return localRanking;
    }

    /** High-level visible method 
     * Daily outgoing trade amount in USD utilising agreedFx
     * @param All trade instructions 
     * @return Collection showing total amount for a given date
     */
    public static Map<LocalDate, BigDecimal> calculateDailyOutgoingAmount(Set<TradeInstruction> instructions) {
        return calculateDailyTradeAmount(instructions, outgoingTrade);
    }

    /** High-level visible method 
     * Daily incoming trade amount in USD utilising agreedFx
     * @param All trade instructions 
     * @return Collection showing total amount for a given date
     */
    public static Map<LocalDate, BigDecimal> calculateDailyIncomingAmount(Set<TradeInstruction> instructions) {
        return calculateDailyTradeAmount(instructions, incomingTrade);
    }

    /**
     * High-level visible method 
     * Daily outgoing trade amount's rank
     * @param All trade instructions 
     * @return Collection showing rankings for a given date
     */
    public static Map<LocalDate, LinkedList<Rank>> calculateDailyOutgoingRanking(Set<TradeInstruction> instructions) {
        return calculateTradeRanking(instructions, outgoingTrade);
    }

    /** High-level visible method 
     * Daily incoming trade amount's rank
     * @param All trade instructions 
     * @return Collection showing rankings for a given date
     */
    public static Map<LocalDate, LinkedList<Rank>> calculateDailyIncomingRanking(Set<TradeInstruction> instructions) {
        return calculateTradeRanking(instructions, incomingTrade);
    }

}
