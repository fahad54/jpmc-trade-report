package co.uk.jpmc.service;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import co.uk.jpmc.beans.Rank;
import co.uk.jpmc.beans.TradeInstruction;

/**
 * Old school database or shell style output elements adopted for reporting
 */
public class ReportGenerationService {
    /** Using {@link StringBuilder} and not {@link StringBuffer} which  is thread safe, based on allTradeInstructions as:
     * "You can assume the code will only ever be executed in a single threaded environment" */
    private StringBuilder finalReport = new StringBuilder();
    
    public void cleanReport() {
    	this.finalReport = new StringBuilder();
    }

    /** Low-level hidden implementation that actually prints report for outgoing rankings
     * @param allTradeInstructions set of outgoing trades
     * @param report The finalReport object that this report appends
     * @return Appended finalReport with outgoing rankings
     */
    private static StringBuilder generateDailyOutgoingTradeRanking(Set<TradeInstruction> allTradeInstructions, StringBuilder report) {
        final Map<LocalDate, LinkedList<Rank>> calculatedDailyOutgoingTrade = SettlementCalculationService.calculateDailyOutgoingRanking(allTradeInstructions);
        report
        .append("\n****************************************\n")
        .append("         Outgoing Daily Ranking          \n")
        .append("****************************************\n")
        .append("     Date    |     Rank   |    Entity    \n")
        .append("****************************************\n");

        for (LocalDate date : calculatedDailyOutgoingTrade.keySet()) {
            for (Rank rank : calculatedDailyOutgoingTrade.get(date)) {
                report
                .append(date).append("   |      ")
                .append(rank.getRank()).append("     |    ")
                .append(rank.getEntity()).append("\n");
            }
        }
        return report;
    }

    /** Low-level hidden implementation that actually prints report for incoming rankings
     * @param allTradeInstructions set of trades
     * @param report The finalReport object that this report appends
     * @return Appended finalReport with incoming rankings
     */
    private static StringBuilder generateDailyIncomingTradeRanking(Set<TradeInstruction> allTradeInstructions, StringBuilder report) {
        final Map<LocalDate, LinkedList<Rank>> calculatedDailyIncomingTrade = SettlementCalculationService.calculateDailyIncomingRanking(allTradeInstructions);
        report
        .append("\n****************************************\n")
        .append("         Incoming Daily Ranking          \n")
        .append("****************************************\n")
        .append("     Date    |     Rank   |    Entity    \n")
        .append("****************************************\n");

        for (LocalDate date : calculatedDailyIncomingTrade.keySet()) {
            for (Rank rank : calculatedDailyIncomingTrade.get(date)) {
                report
                .append(date).append("   |      ")
                .append(rank.getRank()).append("     |    ")
                .append(rank.getEntity()).append("\n");
            }
        }
        return report;
    }

    /** Low-level hidden implementation that actually prints report for outgoing trade amounts
     * @param allTradeInstructions set of trades
     * @param report The finalReport object that this report appends
     * @return Appended finalReport with outgoing trade amounts
     */
    private static StringBuilder generateDailyOutgoingTradeAmount(Set<TradeInstruction> allTradeInstructions, StringBuilder report) {
        final Map<LocalDate, BigDecimal> dailyOutgoingTradeAmount = SettlementCalculationService.calculateDailyOutgoingAmount(allTradeInstructions);
        report
        .append("\n****************************************\n")
        .append("         Outgoing Daily Amount          \n")
        .append("****************************************\n")
        .append("      Date       |    Trade Amount      \n")
        .append("****************************************\n");

        for (LocalDate date : dailyOutgoingTradeAmount.keySet()) {
            report
            .append(date).append("       |      ").append(dailyOutgoingTradeAmount.get(date)).append("\n");
        }
        return report;
    }

    /** Low-level hidden implementation that actually prints report for incoming trade amounts
     * @param allTradeInstructions set of trades
     * @param report The finalReport object that this report appends
     * @return Appended finalReport with incoming trade amounts
     */
    private static StringBuilder generateDailyIncomingTradeAmount(Set<TradeInstruction> tradeInstructions, StringBuilder report) {
        final Map<LocalDate, BigDecimal> dailyOutgoingTradeAmount = SettlementCalculationService.calculateDailyIncomingAmount(tradeInstructions);
        report
        .append("\n****************************************\n")
        .append("         Incoming Daily Amount          \n")
        .append("****************************************\n")
        .append("      Date       |    Trade Amount      \n")
        .append("****************************************\n");

        for (LocalDate date : dailyOutgoingTradeAmount.keySet()) {
            report
            .append(date).append("       |      ").append(dailyOutgoingTradeAmount.get(date)).append("\n");
        }
        return report;
    }

    /** High-level visible method to be called from main program
     * Console based report generation divided into 4 parts based on Amount and Ranking of the Incoming and Outgoing trade instructions */
    public String generateDailyTradeReport(Set<TradeInstruction> allTradeInstructions) {
        SettlementDateService.calculateAllSettlementDates(allTradeInstructions);

        /** Report as normal String for Console printing */
        return generateDailyOutgoingTradeRanking(allTradeInstructions,
            generateDailyIncomingTradeRanking(allTradeInstructions,
                generateDailyIncomingTradeAmount(allTradeInstructions,
                    generateDailyOutgoingTradeAmount(allTradeInstructions, finalReport))))
            .toString();
    }
}
