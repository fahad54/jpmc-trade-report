package co.uk.jpmc.service;

import java.time.LocalDate;
import java.util.Currency;
import java.util.Set;

import co.uk.jpmc.beans.TradeInstruction;


public class SettlementDateService {

    /** Currency dictates which type of work week will be selected
     * Selected currencies from GCC include:
     * Kuwaiti dinar: KWD. Saudi riyal : SAR. United Arab Emirates dirham: AED. Qatari riyal: QAR.
     * @param currency of the instruction
     * @return {@link WorkWeek} based on currency
     */
    private static WorkWeek getWorkWeekBasedOnCurrency(Currency currency) {
        if ((currency.getCurrencyCode().equals("AED")) || (currency.getCurrencyCode().equals("QAR")) || 
            (currency.getCurrencyCode().equals("SAR")) || (currency.getCurrencyCode().equals("KWD"))) {
            return GCCWorkWeek.getGCCWorkWeek();
        }
        return NormalWorkWeek.getNormalWorkWeek();
    }

    /** Ideally this low-level implementation should be hidden from the user, as user don't need to know how we calculate the settlement date
     * Based on working day and work week, we will calculate the settlement date
     * @param tradeInstructions settlement date for a single trade instruction given by the user
     */
    public static void calculateSettlementDate(TradeInstruction tradeInstructions) {
        /** Work week selection based on currency*/
        final WorkWeek currentWorkWeek = getWorkWeekBasedOnCurrency(tradeInstructions.getCurrency());

        /** Calculate new date for settling instruction*/
        final LocalDate calculatedSettlementDate = currentWorkWeek.getWorkingDay(tradeInstructions.getSettlementDate());
        if (calculatedSettlementDate != null) {
            tradeInstructions.setSettlementDate(calculatedSettlementDate);
        }
    }

    /** High-level function to be used in the application
     * @param tradeInstructions the instructions of which the settlement date will be calculated
     */
    public static void calculateAllSettlementDates(Set<TradeInstruction> tradeInstructions) {
        tradeInstructions.forEach(SettlementDateService::calculateSettlementDate);
    }

}
